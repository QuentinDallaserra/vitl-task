export const leftMenuItems = [
  { name: "Vitamins", URL: "/" },
  { name: "DNA", URL: "/" },
  { name: "Blood", URL: "/"
}];

export const rightMenuItems = [
  { name: "Register Kit", URL: "/" },
  { name: "Science", URL: "/" },
  { name: "Account", URL: "/"
}];
