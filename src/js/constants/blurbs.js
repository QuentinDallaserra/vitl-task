export const blurbs = [
  {
    images: ["/img/content/content-blurb.png", "/img/content/content-blurb@2x.png"],
    title: "Complete a consultation",
    content: "vitamins and supplements that suit your unique nutritional profile, to help you feel 100% more often."
  },
  {
    images: ["/img/content/content-blurb.png", "/img/content/content-blurb@2x.png"],
    title: "Review the results",
    content: "vitamins and supplements that suit your unique nutritional profile, to help you feel 100% more often."
  },
  {
    images: ["/img/content/content-blurb.png", "/img/content/content-blurb@2x.png"],
    title: "Order your pack",
    content: "vitamins and supplements that suit your unique nutritional profile, to help you feel 100% more often."
  }
];
