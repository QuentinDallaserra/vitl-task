import React, { Component } from "react";

import Home from "./views/Home";
import Modal from "./components/Modal";
import Header from "./components/Header";
import MainContent from "./components/MainContent";
import Newsletter from "./components/modals/Newsletter";

import "../scss/index.scss";

class App extends Component {
  state = {
    headerHeight: 0
  };

  setHeaderHeight = header => {
    this.setState({ headerHeight: header.offsetHeight });
  };

  render() {
    return (
      <div className="App">
        <Header getHeight={event => this.setHeaderHeight(event)} />

        <MainContent>
          <Home paddingTop={this.state.headerHeight} />
        </MainContent>

        <Modal visible>
          <Newsletter />
        </Modal>
      </div>
    );
  }
}

export default App;
