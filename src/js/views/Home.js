import React, { Component } from "react";

import CallToAction from "../components/templates/CallToAction";
import Blurbs from "../components/templates/Blurbs";
import Information from "../components/templates/Information";
import Section from "../components/Section";

class Home extends Component {
  render() {
    return (
      <>
        <Section 
          paddingTop={this.props.paddingTop}
          backgroundImage="/img/backgrounds/background-petamins.png"
          maxWidth="max"
        >
          <CallToAction />
        </Section>
        <Section backgroundColor="off-white" maxWidth="max">
          <Blurbs />
        </Section>
        <Section backgroundColor="white" maxWidth="max">
          <Information />
        </Section>
      </>
    );
  }
}

export default Home;
