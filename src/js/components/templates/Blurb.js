import React from "react";
import Heading from "../Heading";
import Copy from "../Copy";
import Image from "../Image";

const Blurb = ({ images, title, content }) => {
  return (
    <div className="blurb">
      <div className="blurb__image">
        <Image
          src={images}
          alt={title}
        />
      </div>
      <Heading
        divider="yellow"
        tag="h4"
        title={title}
        size="extra-small"
      />
      <Copy content={[{ size: "small", text: content }]} />
    </div>
  );
};

export default Blurb;
