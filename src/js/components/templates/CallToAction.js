import React from "react";
import Button from "../Button";
import Heading from "../Heading";
import Copy from "../Copy";
import Image from "../Image";

const CallToAction = () => {
  return (
    <div className="call-to-action">

      <div className="call-to-action__image">
        <Image src={["/img/content/content-petamins.png", "/img/content/content-petamins@2x.png"]} alt="Petamins" />
      </div>

      <div className="call-to-action__content">    
        <Heading 
          tag="h1"
          divider="black"
          title="Petamins"
          size="large"
        />
        <Copy
          content={[
            { text: "Vitamins tailored to your pets" },
            { text: "£30 a month" }
          ]}
        />  
        <Button 
          theme="inverted"
          label="Buy now"
          link="/"
        />
      </div>
      
    </div>
  );
};

export default CallToAction;
