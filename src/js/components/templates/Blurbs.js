import React from "react";
import Heading from "../Heading";
import Blurb from "./Blurb";
import { blurbs } from "../../constants/blurbs";

const Blurbs = () => {
  return (
    <div className="blurbs">
      <Heading
        title="How it works"
        marginBottom="extra-large"
      />
      <div className="blurbs__content">
        {blurbs.map(blurb => (
          <Blurb
            images={blurb.images}
            title={blurb.title}
            content={blurb.content}
            key={blurb.title}
          />
        ))}
      </div>
    </div>
  );
};

export default Blurbs;
