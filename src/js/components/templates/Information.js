import React from "react";
import Heading from "../Heading";
import Copy from "../Copy";
import Image from "../Image";

const Information = () => {
  return (
    <div className="information">

      <div className="information__image">
        <Image src={["/img/content/content-information.png", "/img/content/content-information@2x.png"]} alt="Petamins" />
      </div>

      <div className="information__content">
        <Heading 
          divider="yellow"
          title="Made for your pet"
        />
        <Copy 
          content={[{ text: "We believe you need what's best for your health. That's why we only recommend tailor-made vitamins and supplements that suit your unique nutritional profile, to help you feel 100% more often." }]}
        />
      </div>

    </div>
  );
};

export default Information;
