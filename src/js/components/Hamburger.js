import React from "react";

const Hamburger = () => {
  return (
    <div className="hamburger">
      <span></span>
      <span></span>
      <span></span>
    </div>
  );
};

export default Hamburger;
