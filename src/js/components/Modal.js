import React, { Component } from "react";

class Modal extends Component {
  state = {
    hasOpacity: false,
    visible: false
  };

  handleOpenModal = () => {
    this.setState({ visible: true }, () =>
      setTimeout(() => {
        this.setState({ hasOpacity: true });
      }, 0)
    );
  };

  handleCloseModal = () => {
    this.setState({ hasOpacity: false }, () =>
      setTimeout(() => {
        this.setState({ visible: false });
      }, 300)
    );
  };

  componentDidMount() {
    this.props.visible && this.handleOpenModal();
  }

  render() {
    const { hasOpacity, visible } = this.state;
    const { children } = this.props;
    return visible ? (
      <aside className={`modal ${hasOpacity ? "modal--hasOpacity" : ""}`}>
        <div className="modal__container">
          <div className="modal__content">{children}</div>
          <button className="modal__close" onClick={this.handleCloseModal}></button>
        </div>
      </aside>
    ) : (
      <></>
    );
  }
}

export default Modal;
