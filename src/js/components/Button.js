import React from "react";

const Button = ({ label, link, theme = 'primary' }) => {
  const Tag = link ? "a" : "button";

  return (
    <Tag className={`button button--${theme}`} href={link}>
      {label}
    </Tag>
  );
};

export default Button;
