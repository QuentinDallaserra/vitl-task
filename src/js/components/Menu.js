import React from "react";
import Image from "./Image";
import Hamburger from "./Hamburger";
import { leftMenuItems, rightMenuItems } from "../constants/navigation";

const generateMenuList = (listItems, alignment) => (
  <ul className={`menu__list menu__list--align-${alignment}`}>
    {listItems.map(item => (
      <li key={item.name} className="menu__item">
        <a href={item.URL} className="menu__link">{item.name}</a>
      </li>
    ))}
  </ul>
);

const Menu = () => {
  return (
    <nav className="menu site-width site-width--max">
      {generateMenuList(leftMenuItems, "left")}
      <a href="/" className="menu__logo">
        <Image src={["/img/logos/logo-menu.png", "/img/logos/logo-menu@2x.png"]} alt="Vitl Menu Logo" />
      </a>
      {generateMenuList(rightMenuItems, "right")}
      <Hamburger />
    </nav>
  );
};

export default Menu;
