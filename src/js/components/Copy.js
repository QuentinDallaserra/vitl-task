import React from "react";

const Copy = ({ content }) => {
  return (
    <div className="copy">
      {content.map(paragraph => (
        <p className={`copy__paragraph copy__paragraph--size-${paragraph.size || "medium"} copy__paragraph--weight-${paragraph.weight || "regular"}`} key={paragraph.text}>{paragraph.text}</p>
      ))}
    </div>
  );
};

export default Copy;
