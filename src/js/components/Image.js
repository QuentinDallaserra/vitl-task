import React from "react";
import PropTypes from "prop-types";

const Image = ({ className = "", alt = "Image", src }) => {
  return (
    <img
      className={`image ${className}`}
      src={src[0]}
      srcSet={src.map((img, index) => {
        return `${img} ${index + 1}x`;
      })}
      alt={alt}
    />
  );
};

Image.propTypes = {
  src: PropTypes.array.isRequired
};

export default Image;
