import React from "react";

const Heading = ({
  title,
  weight = "regular",
  size = "medium",
  marginBottom = "default",
  divider,
  tag
}) => {
  const Tag = tag || "h3";

  return (
    <>
      <Tag className={`heading heading--size-${size} heading--weight-${weight} heading--margin-bottom-${marginBottom}`}>{title}</Tag>
      {divider && <div className={`heading__divider heading__divider--${divider}`}></div>}
    </>
  );
};

export default Heading;
