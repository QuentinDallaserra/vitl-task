import React from "react";
import Button from "../Button";
import Heading from "../Heading";
import Copy from "../Copy";

const Newsletter = () => {
  return (
    <div className="newsletter">
      
      <div className="newsletter__image" style={{ backgroundImage: "url('/img/content/content-information.png')" }}></div>
      
      <div className="newsletter__content">
        <Heading 
          size="small"
          title="Welcome offer"
          marginBottom="small"
        />
        <Copy
          content={[
            {weight: "bold", text: "Limited time only"},
          ]}
        />
        <Copy
          content={[
            {text: "Subscribe today and get"},
            {size: "super", weight: "bold", text: "50% off"},
            {text: "your first order box"}
          ]}
        />
        <Button label="Buy now" />
        <Button theme="minimal" label="Maybe later" />
      </div>
      
    </div>
  );
};

export default Newsletter;
