import React, { Component } from "react";

class Section extends Component {
  render() {

    const { 
      children,
      backgroundColor,
      maxWidth,
      backgroundImage,
      paddingTop
    } = this.props;

    const backgroundClass = backgroundImage ? "background-image" : "";
    const backgroundURL = backgroundImage || "";

    return (
      <section
        className={`section site-gutter background-color--${backgroundColor || "white"} ${backgroundClass}`}
        style={{ paddingTop: paddingTop, backgroundImage: `url(${backgroundURL})` }}
      >
        <div className={`section__content site-width site-width--${maxWidth || "full"}`}>
          {children}
        </div>
      </section>
    );
  }
}

export default Section;
