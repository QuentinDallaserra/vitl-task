import React, { Component } from "react";

class MainContent extends Component {
  render() {
    const { children, paddingTop } = this.props;
    return (
      <div className="main-content" style={{ paddingTop }}>
        {children}
      </div>
    );
  }
}

export default MainContent;
