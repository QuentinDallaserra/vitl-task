import React, { Component } from "react";
import Menu from "./Menu";

class Header extends Component {
  state = {
    reduced: false
  };

  reduceHeader() {
    this.setState({ reduced: window.pageYOffset > 0 });
  }

  componentDidMount() {
    setTimeout(() => {this.props.getHeight(this.header)}, 0);
    window.addEventListener("scroll", () => this.reduceHeader());
  }

  render() {
    return (
      <header className={`header site-gutter ${this.state.reduced ? "header--reduced" : ""}`} ref={header => (this.header = header)}>
        <Menu />
      </header>
    );
  }
}

export default Header;
